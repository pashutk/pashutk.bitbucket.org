(function() {
  function inherits(childCtor, parentCtor) {
    function tempCtor() {};
    tempCtor.prototype = parentCtor.prototype;
    childCtor.superClass_ = parentCtor.prototype;
    childCtor.prototype = new tempCtor();
    childCtor.prototype.constructor = childCtor;

    childCtor.base = function(me, methodName, var_args) {
      var args = Array.prototype.slice.call(arguments, 2);
      return parentCtor.prototype[methodName].apply(me, args);
    };
  }

  function base(me, opt_method) {
    var caller = arguments.callee.caller;
    if (caller.superClass_) {
      var ctorArgs = Array.prototype.slice.call(arguments, 1);
      return caller.superClass_.constructor.apply(me, ctorArgs);
    }
    var args = Array.prototype.slice.call(arguments, 2);
    var foundCaller = false;
    for (var ctor = me.constructor;ctor;ctor = ctor.superClass_ && ctor.superClass_.constructor) {
      if (ctor.prototype[opt_methodName] === caller) {
        foundCaller = true;
      } else {
        if (foundCaller) {
          return ctor.prototype[opt_methodName].apply(me, args);
        }
      }
    }
    if (me[opt_methodName] === caller) {
      return me.constructor.prototype[opt_methodName].apply(me, args);
    } else {
      throw Error("base called from a method of one name " + "to a method of a different name");
    }
  }

  var EventTarget = function() {
    this.listeners = {};
  };

  EventTarget.prototype.listeners = null;

  EventTarget.prototype.addEventListener = function(type, handler) {
    if (!this.listeners[type]) {
      this.listeners[type] = [];
    }
    this.listeners[type].push(handler);
  };

  EventTarget.prototype.removeEventListener = function(type, handler) {
    if (!this.listeners[type]) {
      return;
    }
    var i;
    for (i = 0; i < this.listeners[type].length; i++) {
      if (this.listeners[type][index] === handler) {
        this.listeners[type].splice(index, 1);
        i--;
      }
    }
  };

  EventTarget.prototype.dispatchEvent = function(event) {
    if (!this.listeners[event.type]) {
      return;
    }
    var i;
    for (i = 0; i < this.listeners[event.type].length; i++) {
      this.listeners[event.type][i].call(this, event);
    }
  };

  var Event = function(type) {
    if (!type) {
      throw new Error('Type missed');
    }
    this.type = type;
  };

  var Application = function() {
    base(this);
  };
  inherits(Application, EventTarget);

  var getDebouncedFunction = function(func, interval, opt_ctx) {
    var timer = null;

    return function() {
      var args = arguments;
      var ctx = opt_ctx || this;

      if (timer !== null) {
        clearTimeout(timer);
      }

      timer = setTimeout(function() {
        func.apply(ctx, args);
        timer = null;
      }, interval);
    };
  };

  window.appApi = {
    Application: Application,
    EventTarget: EventTarget,
    Event: Event,
    inherits: inherits,
    getDebouncedFunction: getDebouncedFunction,
    base: base
  }
})();