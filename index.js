(function() {
  /** 
   * @param {Waypoint.DataType} data
   * @constructor
   */
  var Waypoint = function(data) {
    this.lat = data.lat;
    this.lng = data.lng;
    this.address = data.address;
    this.htmlAddress = data.htmlAddress;
  };

  /**
   * @typedef {{
   *   lat: number,
   *   lng: number,
   *   address: string,
   *   htmlAddress: string
   * }}
   */
  Waypoint.DataType;

  /** 
   * @constructor 
   * @extends {Application}
   */
  var App = function() {
    window.appApi.base(this);

    /** @private {App.AppStep} */
    this._step = null;

    /** @private {Waypoint.DataType} */
    this._origin = null;

    /** @private {Waypoint.DataType} */
    this._destination = null;

    /** @private {google.maps.DirectionsService} */
    this._directionsService = null;

    /** @private {google.maps.DirectionsRenderer} */
    this._directionsRenderer = null;

    /** @private {google.maps.Marker} */
    this._originMarker = null;

    document.getElementById(App.ControlId.SUBMIT).addEventListener('click', this._submitButtonHandler.bind(this));

    this.setStep(App.AppStep.SELECT_ORIGIN);
  };
  window.appApi.inherits(App, window.appApi.Application);

  /** @const {number} */
  App.MAP_JUMP_DELTA = 0.001;

  /** @const {string} */
  App.BASE_API_URL = 'http://localhost:8081/api/request';

  /** @enum {string} */
  App.ControlId = {
    STATUS: 'status',
    ORIGIN: 'origin',
    DESTINATION: 'destination',
    SUBMIT: 'submit',
    MARKER: 'map-marker'
  }

  /** @enum {string} */
  App.AppEventType = {
    MAP_CENTER_CHANGE_DONE: 'map_center_changed',
    MAP_CENTER_CHANGE: 'map_center_change_start',
    MAP_SEARCH_DETAILS: 'map_search_details',
    SEARCH_SUBMIT_BUTTON_PRESSED: 'search_submit_button_pressed'
  };

  /** @enum {string} */
  App.AppStep = {
    SELECT_ORIGIN: 'select_origin',
    SELECT_DESTINATION: 'select_destination',
    LOAD_RESULTS: 'load_results',
    RESULT: 'result'
  };

  /** @enum {string} */
  App.StatusText = {
    SELECT_ORIGIN: 'Откуда едем?',
    SELECT_DESTINATION: 'Куда едем?',
    LOAD_RESULTS: 'Загружаем результаты',
    RESULTS: 'Зацени чо нашли'
  };

  /** @param {google.maps.Map} map */
  App.prototype.setMap = function(map) {
    this._map = map;
  };

  /** @return {google.maps.Map} */
  App.prototype.getMap = function() {
    return this._map;
  };

  /** @param {App.AppStep} step */
  App.prototype.setStep = function(step) {
    this._step = step;

    switch (step) {
      case App.AppStep.SELECT_ORIGIN:
        this.setStatusText(App.StatusText.SELECT_ORIGIN);
        this.showDestination(false);
        break;
      case App.AppStep.SELECT_DESTINATION:
        this.setStatusText(App.StatusText.SELECT_DESTINATION);
        this.showDestination(true);
        this.setOriginMarker(this._origin);
        this.shiftMap();
        break;
      case App.AppStep.LOAD_RESULTS:
        this.setStatusText(App.StatusText.LOAD_RESULTS);
        this.calcRoute(this._origin, this._destination);
        this.hideMainMarker();
        this.hideOriginMarker();
        break;
      case App.AppStep.RESULT:
        this.setStatusText(App.StatusText.RESULT);
        break;
    }
  };

  App.prototype.calcRoute = function(origin, destination) {
    var start = new google.maps.LatLng(origin.lat, origin.lng);
    var end = new google.maps.LatLng(destination.lat, destination.lng);
    var request = {
      origin: start,
      destination: end,
      travelMode: google.maps.TravelMode.DRIVING
    };

    var directionsService = this.getDirectionService();
    directionsService.route(request, this.directionsRouteHandler.bind(this));
  };

  App.prototype.directionsRouteHandler = function(result, status) {
    var directionsDisplay = this.getDirectionsRenderer();
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(result);
      this.sendApiRequest(this.renderApiResults.bind(this));
    }
  };

  App.prototype.sendApiRequest = function(callback) {
    var oLat = this._origin.lat;
    var oLng = this._origin.lng;
    var dLat = this._destination.lat;
    var dLng = this._destination.lng;
    var url = App.BASE_API_URL +
        '?olat=' + oLat +
        '&olng=' + oLng +
        '&dlat=' + dLat +
        '&dlng=' + dLng;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.send();
    xhr.onreadystatechange = function() {
      if (xhr.readyState != 4) return;
      if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
      } else {
        callback(xhr.responseText);
      }
    }
  };

  App.prototype.renderApiResults = function(data) {
    this.setStep(App.AppStep.RESULT);
    document.getElementById(App.ControlId.STATUS).innerText = data;
  };

  App.prototype.hideMainMarker = function() {
    document.getElementById(App.ControlId.MARKER).classList.add('hidden');
  };

  /** @return {google.maps.DirectionsService} */
  App.prototype.getDirectionService = function() {
    return this._directionsService;
  };

  /** @param {google.maps.DirectionsService} service */
  App.prototype.setDirectionService = function(service) {
    this._directionsService = service;
  };

  /** @return {google.maps.DirectionsRenderer} */
  App.prototype.getDirectionsRenderer = function() {
    return this._directionsRenderer;
  };

  /** @param {google.maps.DirectionsRenderer} service */
  App.prototype.setDirectionsRenderer = function(service) {
    this._directionsRenderer = service;
  };

  App.prototype._submitButtonHandler = function() {
    switch (this.getCurrentStep()) {
      case App.AppStep.SELECT_ORIGIN:
        this.setStep(App.AppStep.SELECT_DESTINATION);
        break;
      case App.AppStep.SELECT_DESTINATION:
        this.setStep(App.AppStep.LOAD_RESULTS);
        break;
    }
  };

  App.prototype.shiftMap = function() {
    var map = this.getMap();
    var center = map.getCenter();
    var lat = center.lat();
    var lng = center.lng();
    map.setCenter({
      lat: lat + App.MAP_JUMP_DELTA,
      lng: lng + App.MAP_JUMP_DELTA
    });
  };

  /** @param {App.StatusText} */
  App.prototype.setStatusText = function(statusText) {
    document.getElementById(App.ControlId.STATUS).innerText = statusText;
  };

  /** @param {enable} boolean */
  App.prototype.showDestination = function(enable) {
    var hiddenClassName = 'hidden';
    var destinationClassList = document.getElementById(App.ControlId.DESTINATION).classList;
    if (enable) {
      destinationClassList.remove(hiddenClassName);
    } else {
      destinationClassList.add(hiddenClassName);
    }
  };

  /** @return {App.AppStep} */
  App.prototype.getCurrentStep = function() {
    return this._step;
  };

  /** @param {SearchDetailsEvent} evt */
  App.prototype.setPoint = function(evt) {
    var center = map.getCenter().toJSON();
    var waypoint = new Waypoint({
      lat: center.lat,
      lng: center.lng,
      address: evt.address,
      htmlAddress: evt.htmlAddress
    });
    switch (this.getCurrentStep()) {
      case App.AppStep.SELECT_ORIGIN:
        this._origin = waypoint;
        break;
      case App.AppStep.SELECT_DESTINATION:
        this._destination = waypoint;
        break;
    }
  };

  /** @param {string?} text */
  App.prototype.setWaypointText = function(text) {
    var message = text === null ? '...' : text;
    var elId;
    switch (this.getCurrentStep()) {
      case App.AppStep.SELECT_ORIGIN:
        elId = App.ControlId.ORIGIN;
        break;
      case App.AppStep.SELECT_DESTINATION:
        elId = App.ControlId.DESTINATION;
        break;
      default:
        return;
    }

    document.getElementById(elId).innerText = message;
    document.getElementById(elId).title = message;
  };

  App.prototype.setOriginMarker = function(waypoint) {
    var image = 'http://pashutk.bitbucket.org/map-marker-start.png';
    this._originMarker = marker = new google.maps.Marker({
      position: {
        lat: waypoint.lat,
        lng: waypoint.lng
      },
      map: this.getMap(),
      icon: image,
      animation: google.maps.Animation.DROP
    });
  };

  App.prototype.hideOriginMarker = function() {
    this._originMarker.setMap(null);
  };

  var app;

  window.initMap = function() {
    app = new App();

    var service;
    var directionsDisplay;
    var directionsService;

    var SearchDetailsEvent = function(address, htmlAddress) {
      window.appApi.base(this, App.AppEventType.MAP_SEARCH_DETAILS);
      this.address = address;
      this.htmlAddress = htmlAddress;
    };
    window.appApi.inherits(SearchDetailsEvent, window.appApi.Event);

    function getDetailsHandler(result) {
      if (result) {
        app.dispatchEvent(new SearchDetailsEvent(result['formatted_address'], result['adr_address']));
      } else {
        console.warn('No address from api search');
      }
    }

    function nearbySearchHandler(result, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK &&
          result[0] &&
          result[0]['place_id']) {
        service.getDetails({
          placeId: result[0]['place_id']
        }, getDetailsHandler);
      } else {
        console.warn('No nearby search results');
      }
    }

    /** @param {CenterChangeEvent} evt */
    function mapCenterChangeDoneHandler(evt) {
      var latLng = new google.maps.LatLng(evt.lat, evt.lng);
      if (service) {
        var request = {
          location: latLng,
          rankBy: google.maps.places.RankBy.DISTANCE,
          types: ['address']
        };
        service.nearbySearch(request, nearbySearchHandler);
      }
    }

    /** @param {SearchDetailsEvent} evt */
    function mapSearchDetailsHandler(evt) {
      app.setPoint(evt);
      app.setWaypointText(evt.address);
    }

    /** @param {Event} evt */
    function mapCenterChangeStartHandler() {
      app.setWaypointText(null);
    }

    app.addEventListener(App.AppEventType.MAP_CENTER_CHANGE_DONE, mapCenterChangeDoneHandler);
    app.addEventListener(App.AppEventType.MAP_SEARCH_DETAILS, mapSearchDetailsHandler);
    app.addEventListener(App.AppEventType.MAP_CENTER_CHANGE, mapCenterChangeStartHandler);

    var mapApiCenterChangeDoneHandler = window.appApi.getDebouncedFunction(function() {
      var center = map.getCenter().toJSON();
      app.dispatchEvent(new CenterChangeEvent(center.lat, center.lng));
    }, 300);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 55.7558, lng: 37.6173},
      zoom: 16,
      disableDefaultUI: true,
      zoomControl: true
    });
    app.setMap(map);
    mapApiCenterChangeDoneHandler();

    service = new google.maps.places.PlacesService(map);
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
    directionsService = new google.maps.DirectionsService();
    directionsDisplay.setOptions({
      markerOptions: {
        icon: 'http://pashutk.bitbucket.org/map-marker.png'
      }
    });

    app.setDirectionService(directionsService);
    app.setDirectionsRenderer(directionsDisplay);

    window.map = map;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        map.setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      });
    }

    var CenterChangeEvent = function(lat, lng) {
      window.appApi.base(this, App.AppEventType.MAP_CENTER_CHANGE_DONE);
      this.lat = lat;
      this.lng = lng;
    };
    window.appApi.inherits(CenterChangeEvent, window.appApi.Event);

    function mapApiCenterChangeStartHandler() {
      var type = App.AppEventType.MAP_CENTER_CHANGE;
      app.dispatchEvent(new window.appApi.Event(type));
    }

    map.addListener('center_changed', mapApiCenterChangeStartHandler);
    map.addListener('center_changed', mapApiCenterChangeDoneHandler);
  };
})();